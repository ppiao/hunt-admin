package com.hunt.service.impl;

import com.hunt.service.SmsService;
import com.hunt.support.TaoBaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Map;

/**
 * @Author ouyangan
 * @Date 2017/1/16/14:34
 * @Description
 */
@Service
@Transactional
public class SmsServiceImpl implements SmsService {
    @Autowired
    private TaoBaoService taoBaoService;

    /**
     * @param phone
     * @param map
     * @return
     * @Author ouyangan
     * @Date 2017-1-16 14:33:11
     * @Description 发送单条短信
     */
    @Override
    public void send(String phone, Map<String, String> map) throws Exception {
        taoBaoService.aLiDaYuSendMessage(phone, map);
    }

    /**
     * @param phone
     * @return
     * @Author ouyangan
     * @Date 2017-1-16 14:33:11
     * @Description 查询短信发送记录
     */
    @Override
    public Object selectRecorder(String phone) {
        return null;
    }

    /**
     * @param phones
     * @param map
     * @return
     * @Author ouyangan
     * @Date 2017-1-16 14:33:11
     * @Description 发送多条短信
     */
    @Override
    public Object batchSend(String phones, Map<String, String> map) {
        return null;
    }

}
